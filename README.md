# Power BI Sales Analysis

This project displays how Power Bi could be used to analyse data. The project included
 
* Importing the data
* Creating the Data Relations
* Data Cleaning
* Creating different "calculated columns" and "measures" to get deeper insights from the data
* Creating 3 different reports analysing the sales transactions during the period (1997-1998)


**1.** The first report **(Summary)** shows the summary of the sales with the option to drill through a specific product to navigate to the **"Product"** report page.

![Summary](/uploads/631107e950449acdf676ffc0effac3f1/Summary.PNG)

**2.** The second report **(Product)** includes sales data regarding one specific product

![Product](/uploads/b46e661f5e8d8d1b44b44b07aa97f287/Product.PNG)

**3.**  The last page **(Customer)** shows different sales data for customers
 
![Customer](/uploads/a6b5c44b1115a2973c8f45ba8185dfc5/Customer.PNG)
 
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Power Bi Desktop


### Installing

A step by step series of examples that tell you how to get a development env running

1- Download the Git Repository to a known directory

```
git clone .....
```

2- Open the "PowerBI_Dashboards" within the Dashboard folder 

3- Change the "Data source Settings" to point to the path where the CSV files reside. 


```
go to File -> Options and settings -> Data source settings >Right click data sources and chnage source > browse to the new path.
```

![Data-Source-Settings](/uploads/422ab6a192531d97f492a54933adc248/Data-Source-Settings.PNG)


![Change_Path](/uploads/c5aff26ebfbd5535efabda3cad3d8e7b/Change_Path.png)


4- After changing the file path, You will be able to see the 3 dashboards analysing the sales data.


## Built With

* [Power BI Desktop](https://powerbi.microsoft.com/en-us/desktop/) - Business Intelligence Tool 


## Authors

* **Loay Foda** - *Initial work* - [Loay Foda](https://gitlab.com/loayfoda)


## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details



